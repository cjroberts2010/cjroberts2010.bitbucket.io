var main_8py =
[
    [ "__idx__", "main_8py.html#ac0d4755a56a862032aa87b01ca2f7567", null ],
    [ "__isbroken__", "main_8py.html#a1097576c1a77130c0adc5c5767dcf89c", null ],
    [ "chars_received", "main_8py.html#a16807acfb2325d02ee099ed9474bd0ca", null ],
    [ "controller", "main_8py.html#a6a7c692ddf60a14b3a354fe29476dfcb", null ],
    [ "dataString", "main_8py.html#a452e81df61c03750bd40ed7ed69376b4", null ],
    [ "delta1", "main_8py.html#ad5a1a5233afae752954554d55e1442a1", null ],
    [ "delta2", "main_8py.html#ad2ee16b1513c86859fe751e562b44d6c", null ],
    [ "delta_str", "main_8py.html#afeb01e8c65759e830a7ac1864ae76ebf", null ],
    [ "gotime", "main_8py.html#a04cd4062b6cce9528151f950cb582054", null ],
    [ "in_char", "main_8py.html#ab95760970c65719cfc8ccae42bc3c3c8", null ],
    [ "Kp", "main_8py.html#a912b6ca402799168099201c7ba6476b4", null ],
    [ "L1", "main_8py.html#a0f735b15420a26879def5d939155f241", null ],
    [ "L2", "main_8py.html#a4f49a679a43e71f7eade74e3d7f28476", null ],
    [ "m", "main_8py.html#aeeb741eb24a76142f0b83d5a59f4d580", null ],
    [ "myuart", "main_8py.html#a9be3fc12d7dd7ca0504e230b84d96936", null ],
    [ "n", "main_8py.html#a8dec8a89f8d18a23a0f4a852d596a804", null ],
    [ "omega", "main_8py.html#abf75da43d307151e8298e379f3128860", null ],
    [ "omega_ref", "main_8py.html#a80edbfac8aa013b49a541bbb4f414bc9", null ],
    [ "position", "main_8py.html#ad2e8a6e1705e1aafcd0c9958adc1aae3", null ],
    [ "position_array1", "main_8py.html#a86388615963ce74f0f1951a06b6343f8", null ],
    [ "position_array2", "main_8py.html#aa3a5f3e4c60053e9b2fddcd6db15ae56", null ],
    [ "start_time", "main_8py.html#ae57958345b17f9ca8597330ba07e1a1c", null ],
    [ "state", "main_8py.html#a930442dd832ba9fe94227723c47f6653", null ],
    [ "state0_read", "main_8py.html#a13391ce895c3d73614db84dadef19f11", null ],
    [ "state10_control", "main_8py.html#a2bf4ebce3fbe39a12ca6f58165d0a0f6", null ],
    [ "state1_disperse", "main_8py.html#afdce815a927ad00cba4131e19d622a0a", null ],
    [ "state2_generate", "main_8py.html#aa911272ce2cd969cbdf5b2ca28568a28", null ],
    [ "state3_position", "main_8py.html#aa450c24c2b99b9850570ac4e3dba0dbc", null ],
    [ "state4_zero", "main_8py.html#ab87a6a09ccd3656800b80ee6e9ca1d4e", null ],
    [ "state5_delta", "main_8py.html#a6f90f2a2b6d1117edf57cb0e34204cad", null ],
    [ "state6_run", "main_8py.html#ab4ece0a2f3e994439193232503341507", null ],
    [ "state7_off", "main_8py.html#a8f4730620c2efdbb8e9974b1551d43da", null ],
    [ "state8_velocity", "main_8py.html#abac41e31e5c0613ee567651b1e758148", null ],
    [ "state9_velocity_plot", "main_8py.html#a5dc69fc7a7529ca4cdb280dea9082c72", null ],
    [ "times_array", "main_8py.html#a51e000a3b983efa76ce0976fa9e5db1e", null ],
    [ "vel1", "main_8py.html#a2c337fab1c194b0aa8a0a0ba137150d5", null ],
    [ "vel2", "main_8py.html#a11a583c2c72f45dcfbaf2125f9bbc4e8", null ],
    [ "velocity", "main_8py.html#ae25c8a638d528f443138685401a18212", null ],
    [ "velocity_array1", "main_8py.html#a8bd5696727e6a7bd1e5d098ba66940c2", null ],
    [ "velocity_array2", "main_8py.html#a73d7fbaae56a88edf86ed2cd043a1b87", null ]
];
var classcontrolTask_1_1controlTask =
[
    [ "__init__", "classcontrolTask_1_1controlTask.html#a500e4a52c0c18fccdb8b3079fc4f300f", null ],
    [ "get_position", "classcontrolTask_1_1controlTask.html#a4daf35780218d7cef8a0f98cd5211cd9", null ],
    [ "get_velocity", "classcontrolTask_1_1controlTask.html#ad85f98eda3b43a7cd0e02242dc738d42", null ],
    [ "kill", "classcontrolTask_1_1controlTask.html#a2d0337132141f328cddc438968ddf316", null ],
    [ "motor_enable", "classcontrolTask_1_1controlTask.html#a50e51335bf560cc94934758dc67deb54", null ],
    [ "set_duty", "classcontrolTask_1_1controlTask.html#a1bfdc200ea882b24b6d013f3aad10881", null ],
    [ "update_delta", "classcontrolTask_1_1controlTask.html#abacbbb3f646932fb22f9a8fe38054238", null ],
    [ "update_position", "classcontrolTask_1_1controlTask.html#aadef2d2ca201ce05ca6f8197d1611b82", null ],
    [ "zero", "classcontrolTask_1_1controlTask.html#a30e0841d62fa07e0304334f4d02389b8", null ],
    [ "enc1", "classcontrolTask_1_1controlTask.html#a4439d41b3bd7b7fbf4ca38db21834a73", null ],
    [ "enc2", "classcontrolTask_1_1controlTask.html#a904eb24fdff0034f2dd538fecbf89ff4", null ],
    [ "last_time", "classcontrolTask_1_1controlTask.html#a1aeeae3cd800dee661cf15fba5848149", null ],
    [ "m1", "classcontrolTask_1_1controlTask.html#a6da95db1886962f8d65e9d2951e63e18", null ],
    [ "m2", "classcontrolTask_1_1controlTask.html#a8988c99d9c0894b24b34eb68a9465a6f", null ]
];